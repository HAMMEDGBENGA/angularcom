module.exports=function(){
    return{
        products:[
            {
                id:1,name:"Kayak",category:"Watersports",description:"A boat for one person", price:275, imageurl:"assets\\images\\kayak.jpg"
            },
          
            {
                id:2,name:"Lifejacket",category:"Watersports",description:"Protective and Fashionable", price:25, imageurl:"assets\\images\\lifejacket.jpg"
            },  
            {
                id:3,name:"Soccerball",category:"soccer",description:"FIFA-approved size and weight", price:75, imageurl:"assets\\images\\soccerball.jpg"
            },
            {
                id:4,name:"CornerFlags",category:"soccer",description:"Give your playing field a professional touch", price:24, imageurl:"assets\\images\\cornerflag.jpg"
            },
            {
                id:5,name:"Stadium",category:"soccer",description:"Flat-packed 35,000-seat stadium", price:2750000, imageurl:"assets\\images\\stadium.jpg"
            },
            {
                id:6,name:"Thinking cap",category:"chess",description:"improve brain efficiency by 75%", price:15, imageurl:"assets\\images\\thinkingcap.jpg"
            },
            {
                id:7,name:"Unsteady chair",category:"chess",description:"Secretly give your opponent a disadvantage", price:75, imageurl:"assets\\images\\unsteadychair.jpg"
            },
            {
                id:8,name:"Human chess board",category:"chess",description:"A fun game for the family", price:75, imageurl:"assets\\images\\humanchessboard.jpg"
            },
            {
                id:9,name:"Bling-bling king",category:"chess",description:"Gold-plated, Diamond-studded King", price:1275, imageurl:"assets\\images\\blingbling.jpg"
            },
            {
                id:10, name:"Sexy-bead", category:'bedmatics', description:"kayanmata", price:10, imageurl:"assets\\images\\sexybead.jpg"
            }
        ],
        orders:[]

    }
}