import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { Product } from '../model/product.model';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }
  getProducts():Observable<Product[]>{
    
     return this.http.get("https://localhost:3500/products").pipe(map((res:any)=>{
        return res
      }))
    
  }
}


