import { Injectable } from "@angular/core";
import { Cart } from "./cart.model";
@Injectable()
export class Order{
    public id:any
    public name:any;
    public address:any;
    public city:any;
    public state:any;
    public zip: any;
    public country: any;
    public shipped: boolean=false;
    constructor(public cart:Cart){}
    clear(){
        this.id=0;
        this.name=this.address=this.city=""
        this.state=this.zip=this.country="";
        this.shipped=false;
        this.cart.clear();
    }

}