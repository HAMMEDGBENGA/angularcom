import { HttpClient } from '@angular/common/http';
import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Cart } from '../model/cart.model';
import { Product } from '../model/product.model';
import { ProductRepository } from '../model/product.repository';
import { RestDataSource } from '../model/rest.datasource';
import { ApiService } from '../myservice/api.service';

@Component({
  selector: 'store',
  templateUrl: './store.component.html',
})
export class StoreComponent implements OnInit {
  // public selectedCategory = ''
  public productsPerPage = 4;
  public selectedPage = 1;
  public productList!: Product[];
  public searchinput = '';
  products: Array<any> = [];
  public pageIndex = 1;
  categories: Array<any> = [];
  public productResult: Array<any> = [];
  imageWidth:number=200;
  imageHeight:number=100;
  imageMargin:number=4;
  

  constructor(
    private repository: ProductRepository,
    private cart: Cart,
    private api: ApiService,
    private router: Router,
    private rest: RestDataSource
  ) {}
  getProducts(): Product[] {
    let pageIndex = (this.selectedPage - 1) * this.productsPerPage;
    return this.repository
      .getProducts(this.selectedCategory)
      .slice(pageIndex, pageIndex + this.productsPerPage);
  }
  getCategories(): any {
    return this.repository.getCategories();
  }
  getfilterbycat(category: string) {
    return this.productResult;
  }
  // getCategories(): string[] {
  //     return this.repository.getCategories();

  // }
  changeCategory(newCategory?: any) {
    this.selectedCategory = newCategory;
    console.log(newCategory);
  }

  changePage(newPage: number) {
    this.selectedPage = newPage;
  }
  changePageSize(newSize: number) {
    this.productsPerPage = Number(newSize);
    this.changePage(1);
  }
  getpageNumbers(): number[] {
    return Array(
      Math.ceil(
        this.repository.getProducts(this.selectedCategory).length /
          this.productsPerPage
      )
    )
      .fill(0)
      .map((x, i) => i + 1);
  }
  addProductToCart(product: Product) {
    this.cart.addLine(product);
    this.router.navigateByUrl('/cart');
  }

  ngOnInit(): void {
    this.rest.getProducts().subscribe({
      next: (products) => {
        this.productResult = products;
        this.filteredproduct = this.productResult;
        this.categories = this.productResult
          .map((p) => p.category)
          .filter((c, index, array) => array.indexOf(c) == index)
          .sort();
        console.log(this.productResult);
      },
    });
  }

  private _productfilter: string = '';
  get productfilter(): string {
    return this._productfilter;
  }
  set productfilter(value: string) {
    this._productfilter = value;

    this.filteredproduct = this.getfilterbycat(this.selectedCategory)
      .slice(this.pageIndex, this.pageIndex + this.productsPerPage)
      .filter((p) =>
        p.name.toLocaleLowerCase().includes(value.toLocaleLowerCase())
      );
  }
  private _selectedCategory: string = '';
  get selectedCategory(): string {
    return this._selectedCategory;
  }
  set selectedCategory(value: string) {
    this._selectedCategory = value;

    this.filteredproduct = this.getfilterbycat(this.selectedCategory).filter(
      (p) => p.category == value
    );
  }
  filteredproduct: Array<any> = [];
}

// fetchProductsBySearch(event:any){

//     if(event.target.value==''){

//         this.productResult=this.repository.getProducts(this.selectedCategory)

//     }
//     {
//         this.productResult=this.repository.getProducts(this.selectedCategory).filter((pro)=>{
//             return pro.name?.toLowerCase().startsWith(event.target.value.toLowerCase())
//         })
//         console.log('when input change',this.productResult.length, this.getProducts())

//     }

// }
// performFilter(filterBy:string){
//     filterBy=filterBy.toLocaleLowerCase()
//      this.productResult.filter((p)=>
//         {p.name.toLocaleLowerCase().includes(filterBy)}
//     )
//    }
