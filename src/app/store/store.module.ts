import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { RouterModule } from "@angular/router";
import { ModelModule } from "../model.module";
import { CartDetailsComponent } from "./cartDetails.component";
import { CartSummaryComponent } from "./cartSummary.component";
import { CheckOutComponent } from "./checkOutComponent";
import { StoreComponent } from "./store.component";



@NgModule({
    imports:[ModelModule, BrowserModule,FormsModule, RouterModule],
    declarations:[StoreComponent,CartSummaryComponent,CartDetailsComponent, CheckOutComponent],
    exports:[StoreComponent,CheckOutComponent,CartDetailsComponent]
})
export class StoreModule{}
