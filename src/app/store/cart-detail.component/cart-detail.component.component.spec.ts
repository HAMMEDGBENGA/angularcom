import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CartDetail.ComponentComponent } from './cart-detail.component.component';

describe('CartDetail.ComponentComponent', () => {
  let component: CartDetail.ComponentComponent;
  let fixture: ComponentFixture<CartDetail.ComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CartDetail.ComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CartDetail.ComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
