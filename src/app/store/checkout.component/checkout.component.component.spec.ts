import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Checkout.ComponentComponent } from './checkout.component.component';

describe('Checkout.ComponentComponent', () => {
  let component: Checkout.ComponentComponent;
  let fixture: ComponentFixture<Checkout.ComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Checkout.ComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Checkout.ComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
