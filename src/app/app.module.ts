import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterEvent, RouterModule } from '@angular/router';
import { AdminComponent } from './admin/admin.component';
import { AdminModule } from './admin/admin.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Order } from './model/order.model';
import { OrderRepository } from './model/order.repository';
import { CartDetailsComponent } from './store/cartDetails.component';
import { CheckOutComponent } from './store/checkOutComponent';
import { StoreComponent } from './store/store.component';
import { StoreModule } from './store/store.module';
import { StoreFirstGuard } from './storeFirst.guard';



@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule, HttpClientModule, AppRoutingModule, StoreModule,FormsModule,
    RouterModule.forRoot([
      { path: "store", component: StoreComponent, canActivate: [StoreFirstGuard] },
      { path: "cart", component: CartDetailsComponent, canActivate: [StoreFirstGuard] },
      { path: "checkout", component: CheckOutComponent, canActivate: [StoreFirstGuard] },

      {
        path: "admin", loadChildren: () => import('./admin/admin.module').then(x => x.AdminModule)
        , canActivate: [StoreFirstGuard]
      },
      { path: "**", redirectTo: "/store" }
    ])],



  providers: [StoreFirstGuard, Order, OrderRepository],
  bootstrap: [AppComponent]
})
export class AppModule { }
