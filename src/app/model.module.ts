import { HttpClient } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { Cart } from "./model/cart.model";
import { Order } from "./model/order.model";
import { OrderRepository } from "./model/order.repository";
import { ProductRepository } from "./model/product.repository";
import { StaticDataSource } from "./model/static.datasource";



@NgModule({
    providers:[ProductRepository, StaticDataSource, Cart]
})
export class ModelModule{}